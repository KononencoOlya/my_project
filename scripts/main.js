const tabs = document.querySelector('.tabs');
const tabsTitle = document.querySelectorAll('.tab');
const tabsContent = document.querySelectorAll('.tabs-content-item');

tabs.addEventListener('click', (ev) => {
 const tab = document.getElementById(ev.target.dataset.tab);
 tabsTitle.forEach(item => {
   item.classList.remove('active');
 });
 
 ev.target.classList.add('active');

tabsContent.forEach(item => {
  item.classList.remove('active');
});
 
tab.classList.add('active');
})  


const cardsBtn = document.querySelector('#cardsBtn');
const cardsAmazing = document.querySelectorAll('.our-work-item');
const arr = Array.from(cardsAmazing);
  const arrCards = arr.slice(11, -1)
  arrCards.forEach(el => {
    el.style.display = 'none';
  })
cardsBtn.addEventListener('click', (ev) => {
  arrCards.forEach(el => {
    el.style.display = '';
  })
  cardsBtn.style.display = 'none';
})

let filters = document.querySelectorAll('.menu-item[data-filter]');
let menu = document.querySelector('.menu');

menu.addEventListener('click', (ev) => {
  filters.forEach(item => {
    item.classList.remove('active');
  });
  ev.target.classList.add('active');
  
})
for (let filter of filters) {
    filter.addEventListener('click', function(ev) {
        let catId = filter.getAttribute('data-filter');
        let alldivs = document.querySelectorAll('.our-work-item');

        alldivs.forEach(function (el) {
            if (el.getAttribute('data-cat') == catId){
                el.classList.add('hide');
            } else {
                el.classList.remove('hide');
            }
        })
    });
}

const slider = document.querySelector('.slider');
const imgHead = document.querySelector('#img-head');
const commentsText = document.querySelectorAll('.comments-text');
const sliderItem = document.querySelectorAll('.item-img');
const itenBtn = document.querySelectorAll('.item-btn');
let sliderIndex = 0;

slider.addEventListener('click', (ev) => {
 
  if(ev.target.closest('.item-img')) {
    const activeItem = ev.target.closest('.item-img');
    const indexEl = [...sliderItem].indexOf(activeItem);
    const src =activeItem.children[0].src;
   
     setActive (sliderItem, indexEl);

     imgHead.src = src;
     sliderIndex = indexEl;
     disableBtn(sliderIndex, [...sliderItem]);

    setActive (commentsText, indexEl);
  }

  if(ev.target.closest('.item-btn')) {
    const btn = ev.target.closest('.item-btn');
    if(btn.dataset.btn === 'btn-left') {

      if(sliderIndex === 0) {
        return;
      }
      sliderIndex--;
      disableBtn(sliderIndex, [...sliderItem]);
    }
    if(btn.dataset.btn === 'btn-right') {

      if(sliderIndex === [...sliderItem].length -1) {
        return;
      }
      sliderIndex++;
      disableBtn(sliderIndex, [...sliderItem]);
    }

    setActive(sliderItem, sliderIndex);
    setActive(commentsText, sliderIndex);
    imgHead.src = Array.from(sliderItem)[sliderIndex].children[0].src;
  }

})

function setActive (arr, index) {
  
  arr = Array.isArray(arr) ? arr : Array.from(arr)
  arr.forEach(el => {
    el.classList.remove('active');
  })
  arr[index].classList.add('active');
}

function disableBtn (index, arr ) {
  if(index === 0) {
    document.querySelector('.item-btn[data-btn="btn-left"]').classList.add('btn-disable');
  } else  if(index === arr.length -1) {
    document.querySelector('.item-btn[data-btn="btn-right"]').classList.add('btn-disable');
  } else {
    document.querySelector('.item-btn[data-btn="btn-left"]').classList.remove('btn-disable');
    document.querySelector('.item-btn[data-btn="btn-right"]').classList.remove('btn-disable');
  }
}
